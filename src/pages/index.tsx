import { FC } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { FriendList } from "./friendList";
import { UserProvider } from "../providers/userProvider";
import { UserAdd } from "./userAdd";
import { UserList } from "./userList";
import { Home } from "./home";
import { UserGet } from "./userGet";

export type PagesParams = {};

export const Pages: FC<PagesParams> = () => {
  const userPath = "/users";
  const friendPath = "/friends";
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <UserProvider>
        <Router>
          <Switch>
            <Route path={`${userPath}/:id${friendPath}`}>
              <FriendList />
            </Route>
            <Route path={`${userPath}/add`}>
              <UserAdd />
            </Route>
            <Route path={`${userPath}/:id`}>
              <UserGet />
            </Route>
            <Route path={`${userPath}`}>
              <UserList />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
        <ReactQueryDevtools initialIsOpen={true} />
      </UserProvider>
    </QueryClientProvider>
  );
};
