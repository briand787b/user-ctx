import { useParams } from "react-router-dom";

type idParam = {
  id: string;
}

export const UserGet: React.FC = () => {
  const { id } = useParams<idParam>();

  return <p>User #{id}'s Profile</p>;
};
