import { FC, useContext } from "react";
import { Layout } from "../components/layout";
import { useUsers } from "../hooks/useUsers";
import { UserContext } from "../providers/userProvider";

export const FriendList: FC = () => {
  const user = useContext(UserContext);
  const { users, isError } = useUsers();

  if (isError) {
    return <p>error encountered</p>
  }

  return (
    <Layout fullWidth={true} grid={true}>
      {users.map(u =>
        <div key={u.id}>
          <img src={u.avatarURL} alt={u.lastName}/>
        </div>
      )}
      {user ? (
        <p>
          Welcome, {user.firstName} {user.lastName}{" "}
        </p>
      ) : (
        <p>Please log in</p>
      )}
    </Layout>
  );
};
