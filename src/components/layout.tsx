/** @jsxImportSource @emotion/react */
import { FC } from "react";
import { Header } from "./header";
import { Footer } from "./footer";
import { widths, unit } from "../styles";

export type LayoutParams = {
  children: any;
  fullWidth: boolean;
  grid: boolean;
};

export type PageContainerParams = {
  fullWidth: boolean;
  grid: boolean;
};

export const Layout: FC<LayoutParams> = ({ children, fullWidth, grid }) => {
  return (
    <>
      <Header />
      <div
        css={{
          display: "flex",
          justifyContent: grid ? "center" : "top",
          flexDirection: grid ? "row" : "column",
          flexWrap: "wrap",
          alignSelf: "center",
          flexGrow: 1,
          maxWidth: fullWidth ? "" : `${widths.regularPageWidth}px`,
          width: "100%",
          padding: fullWidth ? 0 : unit * 2,
          paddingBottom: unit * 5,
        }}
      >
        {children}
      </div>
      <Footer />
    </>
  );
};
