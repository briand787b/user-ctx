/** @jsxImportSource @emotion/react */
import { FC, useContext, useEffect, useState } from "react";
import { css } from "@emotion/react";
import { User } from "../ctx/user";
import axios from "axios";
import { UserSetContext } from "../providers/userProvider";

export type HeaderProps = {};

export const Header: FC<HeaderProps> = ({}) => {
  const setUser = useContext(UserSetContext);
  const [users, setUsers] = useState<User[]>([]);
  useEffect(() => {
    (async () => {
      const res = await axios.get<User[]>("http://localhost:3001/users");
      setUsers(res.data);
    })();
  }, [setUser]);

  return (
    <header
      css={css`
        padding: 32px;
        background-color: burlywood;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
      `}
    >
      <h1>Header</h1>
      <select
        onChange={(e) =>
          setUser(users.find((f) => `${f.id}` === e.target.value))
        }
        css={{
          marginLeft: "45%",
        }}
      >
        <option hidden>-- select user --</option>
        {users.map((f) => (
          <option key={f.id} value={f.id}>
            {f.firstName} {f.lastName}
          </option>
        ))}
      </select>
    </header>
  );
};
