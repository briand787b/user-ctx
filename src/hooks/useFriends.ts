import axios from 'axios';
import { useQuery } from 'react-query';

export type Friend = {
  id: number;
  firstName: string;
  lastName: string;
}

type FriendListResponse = {
  id: number;
  userId: number;
  friends: FriendResponse[];
}

type FriendResponse = {
  id: number;
  firstName: string;
  lastName: string;
}

export const useFriends = (userId: number) => {
  const getFriends = async () => {
    const resp = await axios.get<FriendListResponse>(`http://localhost:3001/users/${userId}/friends`)
    return resp.data.friends.map(f => ({...f} as Friend))
  }

  const { data: friends = [], ...query } = useQuery(['friends', userId], getFriends);
  return {
    friends,
    ...query,
  }
};