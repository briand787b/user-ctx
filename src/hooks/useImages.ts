import axios from "axios";
import { useQuery } from 'react-query';

export const UnsplashKeyEnvVar = 'REACT_APP_UNSPLASH_KEY'

export type Photo = {
  id: number;
  url: string;
}

type UnsplashPayload = UnsplashPhoto[];

type UnsplashPhoto = {
  id: string;
  created_at: Date;
  updated_at: Date;
  promoted_at: null;
  width: number;
  height: number;
  color: string;
  blur_hash: string;
  description: null;
  alt_description: string;
  urls: Urls;
  links: Links;
  categories: any[];
  likes: number;
  liked_by_user: boolean;
  current_user_collections: any[];
  sponsorship: Sponsorship;
  user: User;
}

interface Links {
  self: string;
  html: string;
  download: string;
  download_location: string;
}

interface Sponsorship {
  impression_urls: any[];
  tagline: string;
  tagline_url: string;
  sponsor: User;
}

interface User {
  id: string;
  updated_at: Date;
  username: string;
  name: string;
  first_name: string;
  last_name: string;
  twitter_username: string;
  portfolio_url: string;
  bio: string;
  location: null;
  links: UserLinks;
  profile_image: ProfileImage;
  instagram_username: string;
  total_collections: number;
  total_likes: number;
  total_photos: number;
  accepted_tos: boolean;
  for_hire: boolean;
}

interface UserLinks {
  self: string;
  html: string;
  photos: string;
  likes: string;
  portfolio: string;
  following: string;
  followers: string;
}

interface ProfileImage {
  small: string;
  medium: string;
  large: string;
}

interface Urls {
  raw: string;
  full: string;
  regular: string;
  small: string;
  thumb: string;
}


export const useImages = (count: number = 10) => {
  if (count > 30 || count < 1) {
    count = 10;
  }

  // unsplashKey: 1dIS4EFq1Zi9EyQHyT_5ram0GCDuxqVEgBBO30R7g3g
  const query = useQuery('images', async () => {
    const { data } = await axios.get<UnsplashPayload>(`https://api.unsplash.com/photos/?client_id=${process.env[UnsplashKeyEnvVar]}&per_page=${count}`)
    return data.map(p => ({ id: p.id, url: p.urls.small}));
  })

  return {
    images: query.data || [],
    ...query
  }
}