import axios from 'axios';

import { useImages } from './useImages';
import { useQuery } from 'react-query';

export type User = {
  id: number;
  firstName: string;
  lastName: string;
  friendIds: string[];
  avatarURL: string;
}

type ResponseUser = {
  id: number;
  firstName: string;
  lastName: string;
  friendIds: string[];
};

export const useUsers = () => {
  const getUsers = async () => {
    const resp = await axios.get<ResponseUser[]>('http://localhost:3001/users');
    return resp.data;
  }

  const {data: users = [], ...userQuery} = useQuery('users', getUsers);
  const {images = [], ...imageQuery} = useImages(users.length);

  return {
    ...userQuery, 
    users: images.length < users.length ? [] as User[] : users.map((u,i) => ({
      ...u,
      avatarURL: images[i].url,
    })),
  }
};